package model;

import java.io.File;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import model.annotation.MinMaxValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Darab {
    
    private String cim;
    private List<String> szereplok;
    private List<String> rendezok;
    @MinMaxValue(min = 1, max = 10)
    private Integer hossz;
    @MinMaxValue(min = 1, max = 6)
    private Integer felvonas;
    private String leiras;

    public Darab(String cim, List<String> szereplok, List<String> rendezok, Integer hossz, Integer felvonas, String leiras) {
        this.cim = cim;
        this.szereplok = szereplok;
        this.rendezok = rendezok;
        this.hossz = hossz;
        this.felvonas = felvonas;
        this.leiras = leiras;
    }

    public String getCim() {
        return cim;
    }

    public List<String> getSzereplok() {
        return szereplok;
    }

    public List<String> getRendezok() {
        return rendezok;
    }

    public Integer getHossz() {
        return hossz;
    }

    public Integer getFelvonas() {
        return felvonas;
    }

    public String getLeiras() {
        return leiras;
    }

    public void setFelvonas(Integer felvonas) {
        if(felvonas < 1){
            this.felvonas = 1;
        }
        else {
            this.felvonas = felvonas;
        }
    }

    public void setLeiras(String leiras) {
        this.leiras = leiras;
    }
    
    /*
    Darab d = new Darab("skldsdlfs....);
    d.mentes();
    Darab.mentes(d);
    
    Darab d = new Darab("skldsdlfs....);
    d.betoltes();
    
    Darab d = Darab.betoltes();
    
    */
    
    //public static...
    //public static void   hiszen végrehajtó
    //public static Darab  azt a darabot, amit sikerült elmenteni
    //public static Boolean true, ha sikerült létrehozni
    
    public static Boolean mentes(Darab darab){
    
        try{
            String filename = "darab.xml";
            File file = new File(filename);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xml = db.parse(file);
            xml.normalize();
            Element ujDarab = xml.createElement("darab");
            Element cim = xml.createElement("cim");
            Element szereplok = xml.createElement("szereplok");
            Element rendezok = xml.createElement("rendezok");
            Element hossz = xml.createElement("hossz");
            Element felvonas = xml.createElement("felvonas");
            Element leiras = xml.createElement("leiras");
            ujDarab.appendChild(cim);
            ujDarab.appendChild(rendezok);
            ujDarab.appendChild(szereplok);
            ujDarab.appendChild(hossz);
            ujDarab.appendChild(felvonas);
            ujDarab.appendChild(leiras);
            Node darabok = xml.getFirstChild();
            darabok.appendChild(ujDarab);
            cim.setTextContent(darab.cim);
            //cim.appendChild(xml.createTextNode(darab.cim));
            hossz.setTextContent(darab.hossz.toString());
            felvonas.setTextContent(darab.felvonas.toString());
            leiras.setTextContent(darab.leiras);
            for(String rendezoNeve : darab.rendezok){
                Element rendezo = xml.createElement("rendezo");
                rendezok.appendChild(rendezo);
                rendezo.setTextContent(rendezoNeve);
            }
            for(String szereploNeve : darab.szereplok){
                Element szereplo = xml.createElement("szereplo");
                szereplok.appendChild(szereplo);
                szereplo.setTextContent(szereploNeve);
            }
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            DOMSource s = new DOMSource(xml);
            StreamResult r = new StreamResult(file);
            t.transform(s, r);
            return Boolean.TRUE;
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
        
        return Boolean.FALSE;
    }
    
}

package service;

import model.Darab;
import model.annotation.MinMaxValue;

public class DarabService {
    
    public Boolean mentes(Darab darab){
        try{
            Integer minHossz = darab.getClass().getDeclaredField("hossz").getAnnotation(MinMaxValue.class).min();
            Integer maxHossz = darab.getClass().getDeclaredField("hossz").getAnnotation(MinMaxValue.class).max();
            Integer minFelvonas = darab.getClass().getDeclaredField("felvonas").getAnnotation(MinMaxValue.class).min();
            Integer maxFelvonas = darab.getClass().getDeclaredField("felvonas").getAnnotation(MinMaxValue.class).max();
            if(minHossz <= darab.getHossz() && 
               minFelvonas <= darab.getFelvonas() &&
               maxHossz >= darab.getHossz() && 
               maxFelvonas >= darab.getFelvonas()){
                return Darab.mentes(darab);
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
        return Boolean.FALSE;
    }
    
}

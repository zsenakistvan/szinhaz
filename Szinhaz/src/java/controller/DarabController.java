/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Darab;
import service.DarabService;

/**
 *
 * @author zsenakistvan
 */
public class DarabController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        DarabService darabService = new DarabService();
        try{
            if(request.getParameter("feladat") != null){
                if(request.getParameter("feladat").equals("mentes") &&
                   request.getParameter("cim") != null &&
                   request.getParameter("leiras") != null &&
                   request.getParameter("hossz") != null &&
                   request.getParameter("felvonas") != null
                        ){
                    List<String> szereplok = new ArrayList<String>();
                    List<String> rendezok = new ArrayList<String>();
                    szereplok.add("Jóska Pista");
                    szereplok.add("Kis Lóri");
                    rendezok.add("Hold Aladár");
                    rendezok.add("Nap Marcsi");
                    String cim = request.getParameter("cim").trim();
                    String leiras = request.getParameter("leiras").trim();
                    Integer hossz = Integer.parseInt(request.getParameter("hossz").trim());
                    Integer felvonas = Integer.parseInt(request.getParameter("felvonas").trim());
                    Darab darab = new Darab(cim, szereplok, rendezok, hossz, felvonas, leiras);
                    darabService.mentes(darab);
                }
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
